-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--


aAddMenuList = {}
menusWindow = nil;
function onInit()
  Comm.registerSlashHandler("preferences", processPreferences);
  Comm.registerSlashHandler("settings", processPreferences);
  Comm.registerSlashHandler("options", processPreferences);

  OptionsManager.registerOption2("OPTIONS_MENU", true, "option_header_client", "option_label_OPTION_MENU", "option_entry_cycler", 
  { labels = "option_val_sidebar", values = "sidebar", baselabel = "option_val_menus", baseval = "menus", default = "menus" });
end


--[[
  This will add a custom menu/window item to the Menu button.
  
  sRecord           The window class
  sPath             The database node associated with this window (if any)
  sToolTip          The tooltip string record, Interface.getString(sTooltip); Displayed when someone hovers over the menu selection
  sButtonCustomText The text used as the name for the menu. If doesn't exist will look for "library_recordtype_label_" .. sRecord
]]
function addMenuItem(sRecord, sPath, sToolTip, sButtonCustomText)
  local rMenu = {}
  
  rMenu.sRecord           = sRecord;
  rMenu.sPath             = sPath;
  rMenu.sToolTip          = sToolTip
  rMenu.sButtonCustomText = sButtonCustomText;

  table.insert(aAddMenuList,rMenu);
  -- if the menusWindow is already active then
  -- we destroy the list and rebuild adding
  -- the menu just added
  if menusWindow then
    menusWindow.createMenuSelections();
  end
end

function registerMenusWindow(wList)
  menusWindow = wList;
end

function processPreferences()
  Interface.openWindow("options","");
end

wMenuWindow = nil;
function registerWindowMenu(wWindow)
  if not wMenuWindow then
    wMenuWindow = wWindow;
    updateMenuStyle();
  end
end
wSidebarWindow = nil;
function registerWindowSidebar(wWindow)
  if not wSidebarWindow then
    wSidebarWindow = wWindow;
    updateMenuStyle();
  end
end

function updateMenuStyle()
  if wMenuWindow and wSidebarWindow then
    local bMenuStyle = (OptionsManager.getOption("OPTIONS_MENU") == 'menus' or OptionsManager.getOption("OPTIONS_MENU") == '');

    if bMenuStyle then
      enableMenuStyleButtons();
    else
      enableMenuStyleSidebar();
    end
  end
end

function enableMenuStyleButtons()
  wSidebarWindow.setEnabled(false);
  wSidebarWindow.close();
end

function enableMenuStyleSidebar()
  wMenuWindow.setEnabled(false);
  -- and this is required because of the call back mess
  wMenuWindow.close();
end
